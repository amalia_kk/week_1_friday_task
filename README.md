Here I will complete all of the tasks that have been set on Trello.


## <u>What is n OS?</u>

OS stands for Operating System. It is system software that manages computer hardware, software resources, and provides common services for computer programs.


## <u>What is Linux?</u>

Linux is also an opertating system. It is used everywhere, for example smartphones, cars, supercomputers, home appliances, home desktops and enterprise servers. 


### Linux basics

Linux distros (short for distributions) are different versions of Linux so that any type of user can be suited to the system. Popular distros include:
- Linux Mint
- Manjaro
- Debian
- Ubuntu

Each distro has differnces in the desktop, ranging from a more traditional environment to one that is more modern. 


## <u>Terminology: directory and home path</u>

A directory can be thought of as a folder. If you were to ```cd Documents``` in the terminal, you would be changing directory to Documents. The result is that you are in the folder named Documents.

The path can be visualised as the road which takes you from where you currently are to where you want to be. It is displayed as a list, for example ```User/Documents/Work_Documents```.


## <u>Environment Variables</u>

Environment variables are used to pass information, such as user preferred details and special charactersitics, to programs. An example could be myname=Amalia. That way, when myname is called, the information is provided. 

## PATH

PATH is a variable and it is where the shell will search for commands that your user session will want to run without specifying the entire path to the command. If the command is not in the path, you would have to know where the command is and use the full path to run it. To check your path, you can use: 
```echo $PATH```


## <u>Common Commands</u>

These are some commands that may be used regularly:

### User info:
  - id - who you are currently working as
  - who = who is currently logged on 
  - last = who was or is currently on the system
  - date = current time or can be used to change the system's date

### Finding and getting to files
- cd = change directory, puts you in the chosen folder
- pwd = print working directory, shows you were you currently are
- ls = list short, lists the contents of your current directory
- ls -a = the same as ls but shows hidden items
- file = identifies the type of file

### File manipulation
- touch = creates files, can be used to update the time stamp on a file or directory
- cp = copy files or directories
- mv = used to move files or rename them
       mv example Documents/ would move the file called example to the directory Documents
       mx example Documents would rename the file example to Documents
- rm = remove file or directory

### Directory manipulation
- mkdir = creates a directory 
- rmdir = removes a directory


## <u>Wild Cards</u>

